# HIBP-Check
A quick script to check data on HaveIBeenPwned.com using the API
------

    Usage: hibpCheck.py [-h] [-p PASSWORD [PASSWORD ...]]

 optional arguments:

    -h, --help      show this help message and exit
    -p, --password  User is providing a password to hash (using SHA-1 k-Anonymity range)
  

------

From https://haveibeenpwned.com/API/v2#SearchingPwnedPasswordsByRange :

In order to protect the value of the source password being searched for, Pwned Passwords also implements a k-Anonymity model that allows a password to be searched for by partial hash. This allows the first 5 characters of a SHA-1 password hash (not case-sensitive) to be passed to the API.

