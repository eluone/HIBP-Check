# !/usr/bin/python
'HIBP Check Unit Tests'
# Copyright (C) 2020  Tim Cumming
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Author: Tim Cumming
# Created: 05/06/18
# Modified: 29/05/20

import hibpCheck
import unittest


class hashPasswordCheck(unittest.TestCase):
    known_values = (('TEST', '984816FD329622876E14907634264E6F332E9FB3'),
                    ('Password', '8BE3C943B1609FFFBFC51AAD666D0A04ADF83C9D'))

    def test_hashPassword_known_values(self):
        '''hashPassword should give known result with known input'''
        for input, hash in self.known_values:
            result = hibpCheck.hashPassword(input)
            self.assertEqual(hash, result)


class getAllByRangeCheck(unittest.TestCase):
    known_values = (('21BD10018A45C4D1DEF81644B54AB7F969B88D65', 528),
                    ('21BD100D4F6E8FA6EECAD2A3AA415EEC418D38EC', 528),
                    ('984816FD329622876E14907634264E6F332E9FB3', 500),
                    ('8BE3C943B1609FFFBFC51AAD666D0A04ADF83C9D', 521))

    def test_KnownValues(self):
        '''getAllByRange should give known result with known input'''
        for input, hash in self.known_values:
            result, timingMsg = hibpCheck.getAllByRange(input)
            self.assertEqual(hash, len(result))


if __name__ == '__main__':
    unittest.main()
