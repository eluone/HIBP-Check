# !/usr/bin/python
'HIBP Check'
# Copyright (C) 2020  Tim Cumming
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Author: Tim Cumming
# Created: 29/04/18
# Modified: 29/05/20

import argparse
import requests
import hashlib
import time


class APIError(ValueError): pass


version = '0.0.3'
requestHeaders = {'User-Agent': ('hibpCheck/%s +https://gitlab.com/eluone/HIBP-Check' % version)}

# Expected http error codes for this API:
errorFourHundred = "400 - Bad request - the account is not in an acceptable format (empty string?)"
errorFourZeroThree = "403 - Forbidden - Please specify user agent!"
errorFourZeroFour = "404 - the account could not be found and has therefore not been pwned"
errorFourTwoNine = "429 - Rate limit exceeded; refer to acceptable use of the API: https://haveibeenpwned.com/API/v2#AcceptableUse"
errorFiveHundred = "500 - A server error occurred (haveibeenpwned.com) Please try again later."
errorEmailFormat = "The provided string does not appear to be an email address"


# Start of main() function
def main():

    parser = argparse.ArgumentParser(description='HIBP Check - A simple tool to check input provided against HIBP.')

    parser.add_argument("-p", "--password", nargs="+", help="User is providing a password to hash")

    args = parser.parse_args()

    if args.password != None:
        type = "Password"
        for i in args.password:
            fullHash = hashPassword(i)
            output, timingMsg = getAllByRange(fullHash)
            outputString = formatOutput(type, fullHash, output, timingMsg)
            print(outputString)


def hashPassword(password):
    # Generate a SHA1 hash from the supplied input.
    encodedPassword = password.encode("utf-8")
    fullHash = hashlib.sha1(encodedPassword).hexdigest().upper()
    return fullHash


def getAllByRange(fullHash):
    # In order to protect the value of the source password being searched for,
    # Pwned Passwords implements a k-Anonymity model that allows a password to be searched for by partial hash.
    # This allows the first 5 characters of a SHA-1 password hash (not case-sensitive) to be passed to the API
    # https://api.pwnedpasswords.com/range/{first 5 hash chars}

    baseAPIURL = "https://api.pwnedpasswords.com/" # This is the only API URL that is different
    urlEndpoint = "range/"
    rangeHash = fullHash[:5]
    resultDict = {}
    
    if len(rangeHash) == 5:
        urlToFetch = baseAPIURL + urlEndpoint + rangeHash
        
        t = time.process_time() # Start the clock for server response time.
        r = requests.get(urlToFetch, headers=requestHeaders, verify=True)

        if r.status_code == 400:
            raise APIError(errorFourHundred)
        elif r.status_code == 403:
            raise APIError(errorFourZeroThree)
        elif r.status_code == 404:
            raise APIError(errorFourZeroFour)
        elif r.status_code == 429:
            raise APIError(errorFourTwoNine)
        elif r.status_code >= 500:
            raise APIError(errorFiveHundred)
        else:
            resultText = r.text

        timingMsg = '%0.2f ms' % (((time.process_time() - t) * 1000)) # Calculate response time for request.

        # HIBP will return a list of hashes (minus the range prefix)
        # Code below will split the lines then split on the ':' character
        # This will give a dictionary of hashes (key) and how many times (value) they appear in the data.
        d = (resultText).split() # One hash entry per line
        for i in d:
            val = i.split(":")
            resultDict[str(val[0])] = int(val[1])

    return resultDict, timingMsg


def formatOutput(type, input, output, timingMsg):
    
    textList = []

    # Format output from request to string for use in CLI/GUI
    if type == "Password":
        rangeHash = input[:5]
        suffixHash = input[5:]

        textList.append("Full Hash: %s \nFirst Five Characters for Range Search: %s\n\n" % (input, rangeHash))
        textList.append("API Returned %s suffixes that have the prefix %s (Response Time: %s)\n\n" % (len(output), rangeHash, timingMsg))

        if len(output) > 0:
            if suffixHash in output:
                textList.append("Suffix: %s - FOUND in Data Set\n\nThis password has been seen: %s times\nYou should consider changing it.\n" % (suffixHash, output[suffixHash]))
            else:
                textList.append("Suffix: %s\n\nNOT in Data Set / Password NOT Found\n" % suffixHash)
        else:
            textList.append("NO Data Set / Password NOT Found\n")

        textList.append("\nData provided by Have I Been Pwned API. http://haveibeenpwned.com/\n")

    textString = ''.join(textList)
    return textString


# Standard boilerplate to call the main() function to begin
# the program.
if __name__ == '__main__':
    main()
